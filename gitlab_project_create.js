/* 
this file will create project on your gitlab 
this project should be exist on your gitlab before moving your code
*/

const gitlab = require("node-gitlab");

const privateToken = process.env.gitlabPrivateToken || ""; // add you private token from gitlab (account section)
const repoList = ["frontend"]; // these are dummy value, add your existing repo name from vsts (only name not url)
const namespace_id = process.env.gitlabGroupId || 0000; // this is dummy value,add your group id from gitlab (click on group in gitlab , find the id on main page)

const client = gitlab.createPromise({
  api: "https://gitlab.com/api/v4",
  privateToken,
});

const createProj = (name) =>
  client.projects
    .create({ name, namespace_id })
    .then((e) => {
      console.info(`${name} added `);
    })
    .catch((e) => {
      console.error(e);
      if (e.name === "GitlabConnectionTimeoutError") {
        console.info("Retrying ");
        createProj(name);
      }
    });
repoList.forEach((name) => {
  createProj(name);
});
